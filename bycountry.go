package main

import (
	"context"
	"encoding/json"
	"net/http"

	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gocql/gocql"
	"github.com/julienschmidt/httprouter"
	"schneider.vip/problem"
)

type EditorsByCountryResponse struct {
	Items []EditorsByCountry `json:"items"`
}

type EditorsByCountry struct {
	Project       string    `json:"project"`
	ActivityLevel string    `json:"activity-level"`
	Year          string    `json:"year"`
	Month         string    `json:"month"`
	Countries     []Country `json:"Countries"`
}

type Country struct {
	Country     string `json:"country"`
	EditorsCeil int    `json:"editors-ceil"`
}

type EditorsByCountryHandler struct {
	logger  *logger.Logger
	session *gocql.Session
}

func (s *EditorsByCountryHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var err error
	var params = httprouter.ParamsFromContext(r.Context())
	var editorsData = []Country{}
	var response = EditorsByCountryResponse{Items: make([]EditorsByCountry, 0)}

	project := params.ByName("project")
	activityLevel := params.ByName("activity-level")
	year := params.ByName("year")
	month := params.ByName("month")

	ctx := context.Background()

	var countriesJSON string

	query := `SELECT "countriesJSON" FROM "local_group_default_T_editors_bycountry".data WHERE "_domain" = 'analytics.wikimedia.org' and project = ? and "activity-level" = ? AND year = ? AND month = ?`
	if err := s.session.Query(query, project, activityLevel, year, month).WithContext(ctx).Consistency(gocql.One).Scan(&countriesJSON); err != nil {
		s.logger.Request(r).Log(logger.ERROR, "Query failed; %s", err)
		problem.New(
			problem.Type("about:blank"),
			problem.Title(http.StatusText(http.StatusBadRequest)),
			problem.Custom("method", http.MethodGet),
			problem.Status(http.StatusBadRequest),
			problem.Detail(err.Error()),
			problem.Custom("uri", r.RequestURI)).WriteTo(w)
		return
	}

	if err = json.Unmarshal([]byte(countriesJSON), &editorsData); err != nil {
		s.logger.Request(r).Log(logger.ERROR, "Unable to unmarshal returned data: %s", err)
		problem.New(
			problem.Type("about:blank"),
			problem.Title(http.StatusText(http.StatusInternalServerError)),
			problem.Custom("method", http.MethodGet),
			problem.Status(http.StatusInternalServerError),
			problem.Detail(err.Error()),
			problem.Custom("uri", r.RequestURI)).WriteTo(w)
		return
	}

	var countries = []Country{}

	for _, s := range editorsData {
		countries = append(countries, Country{
			Country:     s.Country,
			EditorsCeil: s.EditorsCeil,
		})
	}

	response.Items = append(response.Items, EditorsByCountry{
		Project:       project,
		ActivityLevel: activityLevel,
		Year:          year,
		Month:         month,
		Countries:     countries,
	})

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	var data []byte
	if data, err = json.MarshalIndent(response, "", " "); err != nil {
		s.logger.Request(r).Log(logger.ERROR, "Unable to marshal response object: %s", err)
		problem.New(
			problem.Type("about:blank"),
			problem.Title(http.StatusText(http.StatusInternalServerError)),
			problem.Custom("method", http.MethodGet),
			problem.Status(http.StatusInternalServerError),
			problem.Detail(err.Error()),
			problem.Custom("uri", r.RequestURI)).WriteTo(w)
		return
	}
	w.Write(data)
}
