package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func testURL(suffix string) string {
	var fallback = "http://localhost:8080/metrics/editors"
	var res string
	if res = os.Getenv("API_URL"); res == "" {
		return fmt.Sprintf("%s/%s", fallback, suffix)
	}
	return fmt.Sprintf("%s/%s", strings.TrimRight(res, "/"), suffix)
}

func TestByCountry(t *testing.T) {
	t.Run("returns 200 for valid request", func(t *testing.T) {

		res, err := http.Get(testURL("/by-country/en.wikipedia/5..99-edits/2020/11"))

		require.NoError(t, err, "Invalid http request")

		require.Equal(t, http.StatusOK, res.StatusCode, "Wrong status code")

		body, err := ioutil.ReadAll(res.Body)
		require.NoError(t, err, "Unable to read response")

		n := EditorsByCountryResponse{}
		err = json.Unmarshal(body, &n)

		require.NoError(t, err, "Unable to unmarshall response")

		assert.Len(t, n.Items, 1, "Unexpected response length")

		assert.Contains(t, n.Items[0].Project, "en.wikipedia", "Unexpected response parameters")
		assert.Containsf(t, n.Items[0].Countries[0].Country, "US", "Unexpected response contents")
	})

	t.Run("returns 400 when bycountry project parameter is wrong", func(t *testing.T) {
		res, err := http.Get(testURL("/by-country/WRONGPROJECT/5..99-edits/2020/11"))

		require.NoError(t, err, "Invalid http request")

		require.Equal(t, http.StatusBadRequest, res.StatusCode, "Wrong status code")
	})

	t.Run("returns 400 when bycountry activity parameter is wrong", func(t *testing.T) {

		res, err := http.Get(testURL("/by-country/en.wikipedia/WRONGACTIVITY/2020/11"))

		require.NoError(t, err, "Invalid http request")

		require.Equal(t, http.StatusBadRequest, res.StatusCode, "Wrong status code, %s", err)
	})

	t.Run("returns 400 when bycountry year parameter is wrong", func(t *testing.T) {

		res, err := http.Get(testURL("/by-country/en.wikipedia/5..99-edits/WRONGYEAR/11"))

		require.NoError(t, err, "Invalid http request")

		require.Equal(t, http.StatusBadRequest, res.StatusCode, "Wrong status code")
	})

	t.Run("returns 400 when bycountry month parameter is wrong", func(t *testing.T) {

		res, err := http.Get(testURL("/by-country/en.wikipedia/5..99-edits/2020/WRONGDAY"))

		require.NoError(t, err, "Invalid http request")

		require.Equal(t, http.StatusBadRequest, res.StatusCode, "Wrong status code")
	})

	t.Run("returns 400 when bycountry project parameter is an all-aggregator", func(t *testing.T) {

		res, err := http.Get(testURL("/by-country/all-projects/5..99-edits/2020/11"))

		require.NoError(t, err, "Invalid http request")

		require.Equal(t, http.StatusBadRequest, res.StatusCode, "Wrong status code")
	})

	t.Run("returns 400 when bycountry activity parameter is an all-aggregator", func(t *testing.T) {

		res, err := http.Get(testURL("/by-country/en.wikipedia/all-activity-levels/2020/11"))

		require.NoError(t, err, "Invalid http request")

		require.Equal(t, http.StatusBadRequest, res.StatusCode, "Wrong status code")
	})

	t.Run("returns 400 when bycountry year parameter is an all-aggregator", func(t *testing.T) {

		res, err := http.Get(testURL("/by-country/en.wikipedia/5..99-edits/all-years/11"))

		require.NoError(t, err, "Invalid http request")

		require.Equal(t, http.StatusBadRequest, res.StatusCode, "Wrong status code")
	})

	t.Run("returns 400 when bycountry month parameter is an all-aggregator", func(t *testing.T) {
		res, err := http.Get(testURL("/by-country/en.wikipedia/5..99-edits/2020/all-months"))

		require.NoError(t, err, "Invalid http request")

		require.Equal(t, http.StatusBadRequest, res.StatusCode, "Wrong status code")
	})

	t.Run("does not return data for countries in the deny list", func(t *testing.T) {
		t.Run("returns 200 for valid request", func(t *testing.T) {

			res, err := http.Get(testURL("/by-country/en.wikipedia/5..99-edits/2020/11"))

			require.NoError(t, err, "Invalid http request")

			require.Equal(t, http.StatusOK, res.StatusCode, "Wrong status code")

			body, err := ioutil.ReadAll(res.Body)
			require.NoError(t, err, "Unable to read response")

			n := EditorsByCountryResponse{}
			err = json.Unmarshal(body, &n)

			require.NoError(t, err, "Unable to unmarshall response")

			assert.Len(t, n.Items, 1, "Unexpected response length")

			assert.Contains(t, n.Items[0].Project, "en.wikipedia", "Unexpected response parameters")

			//Check list of returned countries for deny-list data
			for _, s := range n.Items[0].Countries {
				assert.NotContains(t, s.Country, "ER", "Unexpected response contents: results contain deny-list country `ER`")
				assert.NotContains(t, s.Country, "AE", "Unexpected response contents: results contain deny-list country `AE`")
			}
		})
	})
}
